#########################################################################
#
# This Dockerfile intentionally builds a vulnerable project. It was
# created by finding out what advisories were to be published on
# 2024-02-21, and filtered for affected Debian packages mentioned below.
#
#########################################################################

#########################################################################
#
# Image digest resolved by running:
#
# docker pull docker.io/library/debian:10-slim on Mon Feb 5 11:22:04 EST 2024
#
#########################################################################
FROM docker.io/library/debian@sha256:3ab507bedfd2e44b5329130c094953111aa701c264262e0432e4f78b40eb99d8

#########################################################################
#
# All versions of these packages are vulnerable to one of the following
# advisories on Debian 10 (buster).
#
#      cve_id     | package_name | distro_name | distro_version 
# ----------------+--------------+-------------+----------------
#  CVE-2023-4408  | bind9        | debian      | 10
#  CVE-2023-50387 | bind9        | debian      | 10
#  CVE-2023-50868 | bind9        | debian      | 10
#  CVE-2023-5517  | bind9        | debian      | 10
#  CVE-2023-5679  | bind9        | debian      | 10
#  CVE-2023-50387 | dnsmasq      | debian      | 10
#  CVE-2023-50868 | dnsmasq      | debian      | 10
#  CVE-2024-25260 | elfutils     | debian      | 10
#  CVE-2024-1546  | firefox-esr  | debian      | 10
#  CVE-2024-1547  | firefox-esr  | debian      | 10
#
#########################################################################
RUN apt-get update && \
apt-get install --yes --no-install-suggests --no-install-recommends \
  bind9 \
  dnsmasq \
  elfutils \
  firefox-esr
